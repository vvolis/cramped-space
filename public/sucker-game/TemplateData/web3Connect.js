if (window.ethereum) {
  web3 = new Web3(window.ethereum);
  // connect popup
  ethereum.enable();

  window.ethereum.on("accountsChanged", function () {
    location.reload();
  });

}



async function getBlock()
{
    console.log("Get block")
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    //const signer = provider.getSigner()
    var blockNr = await provider.getBlockNumber();
    console.log("Blocknr:", blockNr)
    window.unityInstance.SendMessage('GetBlockBtn', 'SetText', blockNr.toString());
}


async function getEns()
{
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    var ensName = await provider.lookupAddress(web3.currentProvider.selectedAddress);
    var name = web3.currentProvider.selectedAddress;
    name = name.toString();
    if (ensName != null) {
        name = ensName;
    } else {
        ensName = "no ensname found";
    }

    window.unityInstance.SendMessage('GetEnsBtn', 'SetText', ensName);
    window.unityInstance.SendMessage('Launcher', 'SetName', name);
}

/*
Ok, cool. I got it working and attached to a button.
And I can tell if MetaMask is currently using Polygon via window.ethereum.chainId, so I can switch off my “add Polygon to MetaMask” button if it’s obviously not needed.
https://forum.moralis.io/t/automatically-populating-metamask-network-settings/1496/7
*/

async function addPolygonTestnetNetwork()
{
  console.log('Adding networ222k');
    try {
        console.log('try network');
        await window.ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: '0x13881' }], // Hexadecimal version of 80001, prefixed with 0x
        });
    } catch (error) {
      console.log('caught MM polygon error', error);
        if (error.code === 4902) {
            try {
                await window.ethereum.request({
                    method: 'wallet_addEthereumChain',
                    params: [{ 
                        chainId: '0x13881', // Hexadecimal version of 80001, prefixed with 0x
                        chainName: "POLYGON Mumbai",
                        nativeCurrency: {
                            name: "MATIC",
                            symbol: "MATIC",
                            decimals: 18,
                        },
                        rpcUrls: ["https://speedy-nodes-nyc.moralis.io/cebf590f4bcd4f12d78ee1d4/polygon/mumbai"],
                        blockExplorerUrls: ["https://explorer-mumbai.maticvigil.com/"],
                        iconUrls: [""],
                
                    }],
                });
            } catch (addError){
                console.log('Did not add network');
            }
        }
    }
}



async function getSecretAndMint()
{
    
}










