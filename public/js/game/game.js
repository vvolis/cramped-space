class GameScreen {
    static parseResponse(data) {
        if (data.type == "new-user") {
            GameScreen.addUser(data);
        } else if (data.type == "user-answered") {
            GameScreen.userAnswered(data);
        } else {
            console.log(data);
        }
    }

    static addUser(data) {
        $("#user-list").append('<li class="user-name-li" data-userName="' + data.userName + '">' + data.userName + '</li>');
    }

    static userAnswered(data) {

        $('*[data-userName="' + data.userName + '"]').addClass('answered');
    }
}