
$('.trivia-button').on('click', function () {
        if ($('.trivia-button.clicked').length < 1) {
            $(this).addClass('clicked');

            $.ajax({
                method: "POST",
                url: "/game/submit",
                data: { user: $('#user-name-hidden').val(), answer: "Boston", gameCode: $('#game-code-hidden').val()  }
            })
            .done(function( msg ) {
                //alert( "Data Saved: " + msg );
                $("#trivia-block").hide();
                $("#wait-for-others").show();
            });
        }
    }
);