<?php


namespace App\Utils;


use Symfony\Component\Filesystem\Filesystem;

class FileDB
{
    private $fileName;
    private $defaultState;
    public function __construct($_fileName, $_defaultState)
    {
        $this->fileName = $_fileName;
        $this->defaultState = $_defaultState;
    }

    public function ReadState()
    {
        if (!file_exists("/var/www/db/" . $this->fileName . ".txt")) {
            $this->StoreState($this->defaultState);
        }
        $data = file_get_contents("/var/www/db/" . $this->fileName . ".txt");
        if (empty($data)) {
            $data = json_encode($this->defaultState);
            $this->StoreState($this->defaultState);
        }
        return json_decode($data, true);
    }

    public function StoreState($state)
    {
        $filesystem = new Filesystem();
        $filesystem->dumpFile("/var/www/db/" . $this->fileName . ".txt", json_encode($state));
    }
}