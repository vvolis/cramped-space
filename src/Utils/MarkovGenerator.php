<?php
namespace App\Utils;

use Symfony\Component\Filesystem\Filesystem;

class MarkovGenerator
{
    const separator = '<';
    const depth = 1;
    const debug = false;
    private $dict = [];

    //Get text
    //Walk through it and make the dictionary
    //Start with word
    //Continue onwards
    function GenerateTable($data)
    {
        $this->dict = [];

        foreach ($data as $text) {
            $wordList = explode(' ',trim($text));
            $wordKey = [];
            for ($i = 0; $i < self::depth; $i++) {
                $wordKey []= null;
            }

            foreach ($wordList as $word) {
                $this->dict[implode($wordKey,self::separator)] []= $word;

                if (count($wordKey) == self::depth) {
                    array_shift($wordKey); //pop front
                }
                $wordKey []= $word;
            }
        }

        $filesystem = new Filesystem();
        $filesystem->dumpFile('/tmp/vvolis_dict.txt', json_encode($this->dict));
    }

    function GetNextWord($prefix)
    {
        self::debug ? dump($prefix) : false;
        if (array_key_exists($prefix, $this->dict)) {
            self::debug ? dump($this->dict[$prefix]) : false;
            return $this->dict[$prefix][array_rand ($this->dict[$prefix], 1)];
        } else {
            return null;
        }
    }



    function GenerateText($symbolCount)
    {
        $res = "";
        for ($i = 0; $i < self::depth; $i++) {
            $prefix []= null;
        }

        while (strlen($res) < $symbolCount) {

            $word = $this->GetNextWord(implode($prefix, self::separator));

            if ($word == null) {
                break;
            }

            $res .= " " . $word;

            array_shift($prefix);
            $prefix []= $word;
        }

        return $res;
    }




}