<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $question;

    private $correctAnswerIdx;

    /**
     * @ORM\Column(type="array")
     */
    private $answers = [];

    //Current questin
    //4 anser posibilities
    //1 correct answer
    //Every user answer

}