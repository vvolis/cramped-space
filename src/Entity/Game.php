<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */

class Game
{
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code;




    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="users_games",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")}
     *      )
     */
    private $users;














    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code): void
    {
        $this->code = $code;
    }

    public function addUser($user): void
    {
        if (!$this->users->contains($user)) {
            $this->users []= $user;
        }

    }

    public function getUsers()
    {
        return $this->users;
    }




    public function processSubmit(User $user, Question $question, $answer)
    {
        //Every user answer
        $userAnswers[$user->getId][$question->getId()] = $answer;

    }

    private $currentQuestion;

    private $question;

    /**
     * @ORM\Column(type="array")
     */
    private $userAnswers = [];

}