<?php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class GameRepository extends EntityRepository
{
    public function getByCode($code)
    {
        return $this->findOneBy(array('code' => $code));
    }
}