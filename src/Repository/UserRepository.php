<?php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\User;

class UserRepository extends EntityRepository
{
    public function getOrCreateByName($name)
    {
        $user =  $this->findOneBy(array('name' => $name));
        if ($user == null) {
            $user = new User();
            $user->setName($name);
            $this->_em->persist($user);
        }

        return $user;
    }
}