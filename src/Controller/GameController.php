<?php
namespace App\Controller;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

use App\Entity\Game;


class GameController extends AbstractController
{
    private $request;

    public function __construct()
    {
        $this->request = Request::createFromGlobals();
    }

    /**
     * @Route("/game")
     */
    public function index()
    {
        $number = random_int(0, 100);

        /*return new Response(
            '<html><body>If I told you where to go, that would really defeat the point: ' . $number . '</body></html>'
        );*/

        //$this->twitter->send("@@@@xxx.");

        return $this->render('game/index.html.twig', [
            // this array defines the variables passed to the template,
            // where the key is the variable name and the value is the variable value
            // (Twig recommends using snake_case variable names: 'foo_bar' instead of 'fooBar')
            //'user_first_name' => $userFirstName,
            //'notifications' => $userNotifications,
        ]);
    }

    /**
     * @Route("/bump")
     */
    public function bump(PublisherInterface $publisher)
    {
        $number = random_int(0, 100);

        $update = new Update(
            'http://example.com/books/1',
            json_encode(['status' => $number])
        );

        $publisher($update);

        // The Publisher service is an invokable object


        return new Response('published!');
    }




    /**
     * @Route("/game/start")
     */
    public function StartGame()
    {
        $game = new Game();
        $code = substr(md5(microtime()),rand(0,26),5);;
        $game->setCode($code);
        $name = "The best gamename";
        $game->setName($name);
        //TODO::Add questions to it (shuffle maybe)
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($game);
        $entityManager->flush();

        return $this->render('game/host.html.twig', [
            'gameCode' => $game->getCode(),
            'gameName' => $game->getName(),
            'game' => $game
        ]);
    }

    /**
     * @Route("/game/join")
     */
    public function JoinGame(PublisherInterface $publisher)
    {
        $request = Request::createFromGlobals();


        $name = $request->request->get('name', 'defaultname');
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->getOrCreateByName($name);
        $gameCode = $request->request->get('gameCode', 'defaultname');


        $game = $this->getDoctrine()
            ->getRepository(Game::class)
            ->getByCode($gameCode);

        $game->addUser($user);



        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($game);
        $entityManager->persist($user);
        $entityManager->flush();

        //TODO::Might be smarter to reload entire userlist?
        //VV::Maybe do it from frontend and this is just trigger
        $update = new Update(
            'game' . $game->getCode(),
            json_encode([
                'type' => 'new-user',
                'userName' => $user->getName()
            ])
        );
        $publisher($update);

        return $this->render('game/client.html.twig',
            [
                'gameCode' => $game->getCode(),
                'gameName' => $game->getName(),
                'userName' => $user->getName(),
                'game' => $game
            ]);
    }


    /**
     * @Route("/game/submit")
     */
    public function SubmitAnswer(PublisherInterface $publisher)
    {
        $request = Request::createFromGlobals();
        $name = $request->request->get('user', 'defaultname');
        $gameCode = $request->request->get('gameCode', 'defaultname');
        $answer = $request->request->get('answer', 'defaultname');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->getOrCreateByName($name);

        $game = $this->getDoctrine()
            ->getRepository(Game::class)
            ->getByCode($gameCode);

        $game->addUser($user);


        $update = new Update(
            'game' . $game->getCode(),
            json_encode([
                'type' => 'user-answered',
                'userName' => $user->getName()
            ])
        );
        $publisher($update);

        $question = new Question();

        $game->processSubmit($user, $question, $answer);


        //Store answer
        //Send notification to all
        //Do game logic (advance to next Q if all answered
        //Sitas butu jsons jaatgriez ari respose
        return new Response('whatevs! user was:' . $name);
    }

    //====================================BASKETBALL=======================================

    /**
     * @Route("/game/basketball")
     */
    public function Basketball(PublisherInterface $publisher)
    {
        return $this->render('@public/games/basketball/index.html');

    }

    /**
     * @Route("/game/basketball/submit")
     */
    public function SubmitScore(PublisherInterface $publisher)
    {
        $request = Request::createFromGlobals();
        $user = $request->request->get('user', 'defaultname');
        $score = intval($request->request->get('score', 0));

        $scores = $this->ReadState();


        //if (!isset($scores[$user])  || $scores[$user] < $score) {
            $scores[$user] = $score;
            $this->StoreState($scores);

            $update = new Update(
                'basketball',
                json_encode($this->GetOrderedHighScore())
            );

            $publisher($update);

            $update = new Update(
                'basketball_last',
                json_encode([$user,$score ])
            );

            $publisher($update);



        //}


        return new JsonResponse("ok");
    }

    /**
     * @Route("/game/basketball/highscore")
     */
    public function HighScore()
    {
        return $this->render('game/basketball/highscore.twig', [
            'scores' => $this->GetOrderedHighScore()
        ]);
    }

    /**
     * @Route("/game/basketball/clear")
     */
    public function Clear()
    {
        $this->StoreState([]);
        return new JsonResponse("ok");
    }

    /**
     * @Route("/game/basketball/ban/{username}")
     */
    public function Ban($username, PublisherInterface $publisher)
    {
        $scores = $this->ReadState();
        unset($scores[$username]);
        $this->StoreState($scores);
        $update = new Update(
            'basketball',
            json_encode($this->GetOrderedHighScore())
        );

        $publisher($update);

        return new JsonResponse("ok");
    }

    private function GetOrderedHighScore()
    {
        $scores = $this->ReadState();

        unset($scores[""]);


        $res = [];
        foreach ($scores as $k => $v) {
            $res []= [$k, $v];
        }

        usort($res, function ($a , $b ) {
            return $a[1] < $b[1];
        });





        $res = array_slice($res, 0, 5); //Top10

        return $res;
    }


    private function ReadState()
    {
        if (!file_exists("/tmp/basketball.txt")) {
            $this->StoreState([]);
        }
        $data = file_get_contents("/tmp/basketball.txt");
        if (empty($data)) {
            $data = [
                'davis' => 1,
                'valters' => 1
            ];
        }
        return json_decode($data, true);
    }

    private function StoreState($state)
    {
        $filesystem = new Filesystem();
        $filesystem->dumpFile('/tmp/basketball.txt', json_encode($state));
    }









}