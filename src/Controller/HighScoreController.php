<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;


class HighScoreController extends AbstractController
{
    /**
     * @Route("/highscores/post")
     */
    public function Post()
    {
        $request = Request::createFromGlobals();
        $key = $request->query->get('key', null);
        $user = $request->query->get('user', 0);
        $score = $request->query->get('score', 0);

        $state = $this->ReadState($key);
        $state['highscores'][$user]['score'] = $score;
        $this->StoreState($state);
        return new Response(json_encode($state)) ;
    }



    private function ReadState($key)
    {
        $data = file_get_contents("/var/www/highscores/" . $key . ".txt");
        if (empty($data)) {
            $data ="[]";
        }
        $data = json_decode($data, true);
        $data['key'] = $key;
        return $data;
    }

    private function StoreState($state)
    {
        $filesystem = new Filesystem();
        $filesystem->dumpFile("/var/www/highscores/" . $state["key"] . ".txt", json_encode($state));
    }



}