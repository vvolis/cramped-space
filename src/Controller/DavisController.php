<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;


class DavisController extends AbstractController
{
    /**
     * @Route("/davis/set-question")
     */
    public function SetQuestion()
    {
        $request = Request::createFromGlobals();
        $q = $request->query->get('q', 'defaultQuestion');
        $a = $request->query->get('a', 'defaultAnswer');


        $state = [
            'q' => $q,
            'a' => $a,
            'show' => false,
            'hide' => false
        ];

        $this->StoreState($state);
        return new Response('Set Q:' . $q . " A:" . $a);
    }

    /**
     * @Route("/davis/show-answer")
     */
    public function ShowAnswer()
    {
        $state = $this->ReadState();
        $state['show'] = true;
        $state['hide'] = false;
        $this->StoreState($state);
        return new Response('Answer showing enabled');
    }

    /**
     * @Route("/davis/show")
     */
    public function Show()
    {
        $state = $this->ReadState();

        return $this->render('davis/index.html.twig', [
            'question' => $state['q'],
            'answer' => $state['a'],
            'show' =>  $state['show'],
            'hide' => $state['hide']

        ]);
    }

    /**
     * @Route("/davis/show2")
     */
    public function Show2()
    {
        $state = $this->ReadState();

        return $this->render('davis/index.html.twig', [
            'question' => $state['q'],
            'answer' => $state['a'],
            'show' =>  $state['show'],
            'hide' => $state['hide']
        ]);
    }

    /**
     * @Route("/davis/hide")
     */
    public function Hide()
    {
        $state = $this->ReadState();
        $state['hide'] = true;
        $this->StoreState($state);
        return new Response('Hiding all');
    }


    private function ReadState()
    {
        $data = file_get_contents("/tmp/davis.txt");
        return json_decode($data, true);
    }

    private function StoreState($state)
    {
        $filesystem = new Filesystem();
        $filesystem->dumpFile('/tmp/davis.txt', json_encode($state));
    }



}