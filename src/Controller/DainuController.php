<?php
namespace App\Controller;

use App\Utils\FileDB;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use DG\Twitter\Twitter;



class DainuController extends AbstractController
{

    private $twitter;

    public function __construct()
    {

        $settings = array(
            'oauth_access_token' => "1158003084439752704-ruh7Y0lYkNm9oOneuFl5n3TaN23NG3",
            'oauth_access_token_secret' => "gTV7ovi25UfXP698YsVccxMGRZy5o9uPeq0K01X3Dnjv9",
            'consumer_key' => "HQ9b80cytFlEEcIhgINyLyYeA",
            'consumer_secret' => "DVW7uUzvorj6YRsFDc2gzr6DXH4slGuiBYUxu5WTukxBRMGUUG"
        );
        $this->twitter = new Twitter($settings['consumer_key'], $settings['consumer_secret'], $settings['oauth_access_token'], $settings['oauth_access_token_secret']);
    }

    /**
     * @Route("/dainas/clear")
     */
    public function Clear()
    {
        $state = $this->ReadState();
        $state["tweets"] = [];
        $this->StoreState($state);

        return new JsonResponse("ok");
    }
        /**
     * @Route("/dainas/check")
     */
    public function Check()
    {

        $state = $this->ReadState();
        dump($state);

        $state["tweets"] = array_values($state["tweets"]);

        $rhymeres = [];


        for( $i = 0; $i < count($state["tweets"]); $i++) {
            for( $j = $i + 1; $j < count($state["tweets"]); $j++) {
                $rhymeScore = $this->Rhymes($state["tweets"][$i]["ritms"]["textSkaniParts"], $state["tweets"][$j]["ritms"]["textSkaniParts"]);
                $rhymeres []= ["score" => $rhymeScore, "A" => $state["tweets"][$i], "B"=> $state["tweets"][$j]];
            }
        }

        $rhymeres = array_filter($rhymeres, function($v)
        {
            return $v["score"] > 1;
        });

        usort($rhymeres, function ($a, $b) { return $a["score"]  <=> $b["score"]; });
        $rhymeres = array_reverse ($rhymeres);

        dump($rhymeres);

        //$state = $this->ReadState();
        //$state["tweets"] = [];
        //$this->StoreState($state);

        return new JsonResponse("ok");
    }

    /**
     * @Route("/dainas/tweet")
     */
    public function Tweet()
    {
        set_time_limit(300);

        $state = $this->ReadState();


        $state["tweets"] = array_values($state["tweets"]);

        $rhymeres = [];


        try {
            for( $i = 0; $i < count($state["tweets"]); $i++) {
                for( $j = $i + 1; $j < count($state["tweets"]); $j++) {
                    $rhymeScore = $this->Rhymes($state["tweets"][$i]["ritms"]["textSkaniParts"], $state["tweets"][$j]["ritms"]["textSkaniParts"]);
                    if ($rhymeScore > 1 && $rhymeScore < 5) {
                        $rhymeres []= ["score" => $rhymeScore, "A" => $state["tweets"][$i], "B"=> $state["tweets"][$j]];
                    }
                    if (count($rhymeres) > 100) {
                        break;
                    }
                }

            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return new JsonResponse("ok");
        }


        usort($rhymeres, function ($a, $b) { return $a["score"]  <=> $b["score"]; });
        $rhymeres = array_reverse ($rhymeres);


        $usedTweets = [];
        $rhymeres = array_values(array_filter($rhymeres, function($v ) use (&$usedTweets)
        {
            //dump($v);
            if (!in_array($v["A"]["tweet"], $usedTweets ) && !in_array($v["B"]["tweet"], $usedTweets )) {
                $usedTweets []= $v["A"]["tweet"];
                $usedTweets []= $v["B"]["tweet"];
                return true;
            }

            return false;
        }));

        for ($i = 0; ($i + 1) < count($rhymeres); $i += 2) {

            $smth = $rhymeres[$i]["A"]["tweet"] . "\n";

            $smth .= $rhymeres[$i + 1]["A"]["tweet"] . "\n";
            $smth .= $rhymeres[$i]["B"]["tweet"] . "\n";
            $smth .= $rhymeres[$i + 1]["B"]["tweet"] . "\n\n";

            $users = ['@' . $rhymeres[$i]["A"]["user"],'@' . $rhymeres[$i]["B"]["user"],
                '@' . $rhymeres[$i + 1]["A"]["user"],
                '@' . $rhymeres[$i + 1]["B"]["user"]];

            $smth .= "RT: ".implode($users, " ");

            //state = $this->ReadState();

            $state["tweets"] = array_filter($state["tweets"], function($tweet) use (&$rhymeres,$i )
            {
                 $isused = in_array($tweet["tweet"], [
                    $rhymeres[$i]["A"]["tweet"],
                    $rhymeres[$i + 1]["A"]["tweet"],
                    $rhymeres[$i]["B"]["tweet"],
                    $rhymeres[$i + 1]["B"]["tweet"]
                ]);

                if ($isused) {
                    dump($tweet["tweet"]);
                }

                return !$isused;
            });

            //********** SEND BLOCK ************

            //Reset the tweet memory on every fill otherwise it goes into a loop and i am too lazy to fix it.
            $state["tweets"] = [];
            $this->StoreState($state);
            $this->twitter->send($smth);
            //***********************************


            break;
        }



        return new Response("ok");
    }

    /**
     * @Route("/dainas/read")
     */
    public function Read()
    {
        $process = true;
        $max_id = 0;

        $notgood = [];
        while ($process) {
            $state = $this->ReadState();

            $params = ['list_id' => 1166077661401296896,
                'count' => 10,
                'since_id' => $state['last_tweet_id'],
                'include_rts' => false,
                'include_entities' => false,
                'tweet_mode' => 'extended'];

            if ($max_id != 0) {
                $params['max_id'] = $max_id;
            }

            try {
                $statuses = (array)$this->twitter->request('lists/statuses', 'GET', $params);
            } catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                return new JsonResponse($e->getMessage());
            } catch (\Throwable $t)
            {
                echo 'Caught exception: ',  $t->getMessage(), "\n";
                return new JsonResponse($t->getMessage());
            }



            foreach ($statuses as $status) {
                $text = $status->full_text;
                while ($text[0] == "@") {
                    $text = substr(strstr($text," "), 1);
                }

                if ($this->IsUsableText($text)) {
                    $rhytm = $this->GetLVRythm($text);

                    $zilbes = mb_strlen($rhytm['ritms']);
                    if ($zilbes == 7 || $zilbes == 8) {
                        $state['tweets'] []= ["tweet" => $text, "user" => $status->user->screen_name, "ritms" => $rhytm];
                    }
                } else {
                    $notgood [] = ["tweet" => $text, "user" => $status->user->screen_name];
                }
                $state['last_tweet_id'] = $state['last_tweet_id'] < $status->id ? $status->id : $state['last_tweet_id'];
            }

            if (count($statuses) == 0) {
                $process = false;
            }

            $this->StoreState($state);
        }

        $state = $this->ReadState();
        $state["tweets"] = array_filter($state["tweets"], function($v)
        {
            return $v["user"] != "unknown";

        });

        $response = new JsonResponse("ok");
        $response->setCharset('UTF-8');

        return $response;
    }

    function GetLVRythm($text)
    {
        $text = $this->NormaliseLidzskani($text);
        $textParts = $this->SpliBySkanis($text);

        $res = ['ritms' => "", "syls" => [], "text" => $text, 'textSkaniParts' => $textParts];
        $pieces = explode(' ', $text);

        foreach ($pieces as $word) {
            $syls = $this->Syllables($word);
            $res["syls"] []= $syls;

            $flip = false;
            for ($i = 0; $i < count($syls); $i++) {
                $res["ritms"] .= ($flip ? "0" : "1");
                $flip = !$flip;
            }

        }

        return $res;
    }

    function NormaliseLidzskani($text)
    {
        //$text[0]

            $text = str_replace("b", "p", $text);
            $text = str_replace("d", "t", $text);
            $text = str_replace("g", "k", $text);
            $text = str_replace("ģ", "ķ", $text);
            $text = str_replace("z", "s", $text);
            $text = str_replace("ž", "š", $text);
            $text = str_replace("dz", "c", $text);
            $text = str_replace("dž", "č", $text);

        return $text;
    }

    function SameSkani($a, $b)
    {
        return ($this->IsLidzskanis($a) && $this->IsLidzskanis($b) ) || ($this->IsPatskanis($a) && $this->IsPatskanis($b));
    }

    function IsLidzskanis($a)
    {
        $lidzskani = preg_split('//u', "bcčdfgģhjkķlļmnņpqrsštvwxzž",-1, PREG_SPLIT_NO_EMPTY);
        return in_array($a, $lidzskani);
    }

    function IsPatskanis($a)
    {
        $patskani = preg_split('//u', "aeiouāēīū", -1, PREG_SPLIT_NO_EMPTY);
        return in_array($a, $patskani);
    }

    function SpliBySkanis($txt)
    {
        $txt = strtolower($txt);
        $txt = preg_replace ("/[^[:alnum:][:space:]]/u", "", $txt);


        $parts = [];
        $part = "";
        $txt2 = preg_split('//u', $txt, -1, PREG_SPLIT_NO_EMPTY);



        $lastletter = "";

        //dump($txt2);
        foreach ($txt2 as $letter)
        {

            if (strlen($part) != 0 && !$this->SameSkani($letter,$lastletter)) {
                $parts []= $part;
                $part = "";

            } else if ($this->IsPatskanis($letter)) {
                if ($letter == $lastletter) {
                    $parts [] = $part;
                    $part = "";
                } else if (!stristr("ai,eu,ie", $lastletter.$letter)) {
                    $parts [] = $part;
                    $part = "";
                }
            }

            $part .= $letter;
            $lastletter = $letter;
        }

        $parts []= $part;

        foreach ($parts as &$tmp) {
            $tmp = str_replace (" ", "", $tmp);
        }
        //dump($parts);

        $parts = array_values(array_filter($parts, function($v)
        {
            return !empty($v);
        }));

        //dump($parts);

        return $parts;
    }

    function Rhymes($skaniPartsA, $skaniPartsB)
    {
        $sylsMax = min (count($skaniPartsA), count($skaniPartsB));
        $res = 0;

        $nextAdd = 1;
        for ($i = 0; $i < $sylsMax; $i++) {
            $aPart = $skaniPartsA[count($skaniPartsA) -$i - 1];
            $bPart = $skaniPartsB[count($skaniPartsB) -$i - 1];

            if ($aPart == $bPart ) {
                $res += $nextAdd;
                $nextAdd = 1;
            } else {
                if ($this->IsPatskanis(mb_substr($aPart, 0,1) ) ){
                    break;
                } else {
                    $nextAdd -= 0.5;
                    if ($nextAdd == 0) {
                        break;
                    }

                }
            }
        }

        return $res;
    }

    function Syllables($txt)
    {
        $parts = $this->SpliBySkanis($txt);

        $syls = [];
        foreach($parts as $idx=>$part) {
            if (strlen($part) == 0) {
                continue;
            }
            if ($this->IsPatskanis(preg_split('//u', $part, -1, PREG_SPLIT_NO_EMPTY)[0])) {
                $syl = $part;
                $prevPart = null;
                $nextPart = null;

                //Steal from behind
                if ($idx !=0 && !empty($parts[$idx - 1]) && $this->IsLidzskanis($parts[$idx - 1][0])) {
                    $prevPart = $parts[$idx - 1];
                    //Takke all starting letters in first syl
                    if ($idx == 1) {
                        $syl = $prevPart . $syl;
                    } else {
                        if (mb_strlen($prevPart) == 1) {
                            $syl = $prevPart . $syl;
                        } else {
                            $takeFromPrev = 0;
                            if (mb_strlen($prevPart) %2 != 0) {
                                $takeFromPrev = (mb_strlen($prevPart) +1) /2.0;
                            } else {
                                $takeFromPrev = mb_strlen($prevPart) /2.0;
                            }
                            $syl = mb_substr($prevPart, -$takeFromPrev ) . $syl;
                        }
                    }
                }

                //Take From the front
                if ($idx != count($parts) - 1 && $this->IsLidzskanis($parts[$idx + 1][0])) {
                    $nextPart = $parts[$idx + 1];
                    //Takke all ending letters in the last syl
                    if ($idx == count($parts) - 2) {
                        $syl = $syl . $nextPart;
                    } else {
                        $takeFromNext = 0;
                        if (mb_strlen($nextPart) %2 != 0) {
                            $takeFromNext = (mb_strlen($nextPart) - 1) /2.0;
                        } else {
                            $takeFromNext = mb_strlen($nextPart) /2.0;
                        }
                        $syl =  $syl . mb_substr($nextPart,0,  $takeFromNext );

                    }
                }

                $syls [] = $syl;
            }
        }

        return $syls;
    }

    function IsUsableText($text)
    {
        $noLinks = !(strpos($text, 'http') !== false);
        $noMentions = !(strpos($text, "@") !== false);
        $noNumbers = !preg_match('~[0-9]+~', $text);
        $atLeastTenSymbols = strlen($text) >=10;

        return $noLinks && $noMentions && $noNumbers && $atLeastTenSymbols&&  !empty($text);
    }

    private function ReadState()
    {
        if (!file_exists("/var/www/db/dainas4.txt")) {
            $this->StoreState([]);
        }
        $data = file_get_contents("/var/www/db/dainas4.txt");
        if (empty(json_decode($data))) {
            $data = json_encode(['last_tweet_id' => 1299687134819999746, 'tweets' => []]);
            $this->StoreState(json_decode($data));
        }
        return json_decode($data, true);
    }

    private function StoreState($state)
    {
        $filesystem = new Filesystem();
        $filesystem->remove(['/var/www/db/dainas4.txt']);
        $filesystem->dumpFile('/var/www/db/dainas4.txt', json_encode($state));
    }

}