<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use Symfony\Component\HttpFoundation\Request;


class DefaultController extends AbstractController
{
    private $request;

    public function __construct()
    {
        $this->request = Request::createFromGlobals();

        $settings = array(
            'oauth_access_token' => "1158003084439752704-ruh7Y0lYkNm9oOneuFl5n3TaN23NG3",
            'oauth_access_token_secret' => "gTV7ovi25UfXP698YsVccxMGRZy5o9uPeq0K01X3Dnjv9",
            'consumer_key' => "HQ9b80cytFlEEcIhgINyLyYeA",
            'consumer_secret' => "DVW7uUzvorj6YRsFDc2gzr6DXH4slGuiBYUxu5WTukxBRMGUUG"
        );


    }


    public function index()
    {

        return $this->render('index.html.twig', [
        ]);
    }

    /**
     * @Route("/vv")
     */
    public function portfolio()
    {
        $projects = [

            [
                'title' => 'NIRFF',
                'img_path' => 'https://i.imgur.com/owwOxkm.png',
                'url' => 'https://nirff.com/',
                'description' => 'Itsd the nothing is real film festival ',
                'img_alt_path' => 'https://i.imgur.com/owwOxkm.png',
            ],

            [
                'title' => 'Kiwie1001',
                'img_path' => 'https://gateway.pinata.cloud/ipfs/QmQ93W1B88U2tKNAe5fe2PbRUnTBP3FPwUJugJN5YZsTrN/RARIBLE-0012-2.gif',
                'url' => 'https://kiwie1001.com/',
                'description' => 'INclude the kiwie project description',
                'img_alt_path' => 'https://gateway.pinata.cloud/ipfs/QmQ93W1B88U2tKNAe5fe2PbRUnTBP3FPwUJugJN5YZsTrN/RARIBLE-0012-2.gif',
            ],

            [
                'title' => 'Real Laumaryn',
                'img_path' => 'https://djalil.chafai.net/blog/wp-content/uploads/2011/09/MarkovChain.jpg',
                'url' => 'https://twitter.com/Real_Laumaryn',
                'description' => 'Markov chain based twitter bot',
                'img_alt_path' => 'https://djalil.chafai.net/blog/wp-content/uploads/2011/09/MarkovChain.jpg',
            ],

            [
                'title' => 'Mobile control',
                'img_path' => 'https://i.imgur.com/9tQp9OI.png',
                'url' => '/games/mobile',
                'description' => 'Mobile control prototype',
                'img_alt_path' => 'https://i.imgur.com/9tQp9OI.png',
            ],



            [
            	'title' => 'Space Chicken',
				'img_path' => 'games/cals_s.gif',
				'url' => '/games/cals',
				'description' => $this->render('portfolio/cals.html')->getContent(),
				'img_alt_path' => 'games/cals.gif',
            ],

            [
				'title' => 'shrooms',
                'img_path' => 'games/shroom_s.png',
                'url' => '/games/shroom',
				'description' => 'Cek aut them shrooms',
				'img_alt_path' => 'games/shroom.png',
            ],

            [
                'title' => 'Basketball',
                'img_path' => 'https://i.imgur.com/RZbzcd0.png',
                'url' => '/games/basketball',
                'description' => 'Basketball game',
                'img_alt_path' => 'https://i.imgur.com/RZbzcd0.png',
            ],

            [
                'title' => 'Botrons',
                'img_path' => 'https://pbs.twimg.com/profile_images/1158003357002457088/m2vfqEkH_400x400.jpg',
                'url' => 'https://twitter.com/dainu__skapis',
                'description' => 'Its the botron boi',
                'img_alt_path' => 'https://pbs.twimg.com/profile_images/1158003357002457088/m2vfqEkH_400x400.jpg',
            ],




			[
				'title' => 'Secrets',
				'img_path' => 'games/whisper_s.png',
				'url' => '/secret',
				'description' => 'A project that runs on secrets',
				'img_alt_path' => 'games/whisper.png',
			],

			[
				'title' => 'Hypersonic',
				'img_path' => 'games/hyper_s.png',
				'url' => 'https://github.com/vvolis/CodinGame',
				'description' => 'A bomberman bot to play Codingames Hypersonic game',
				'img_alt_path' => 'games/hyper.png',
			],

        ];


        return $this->render('portfolio.html.twig', [
			'projects' => $projects
        ]);
    }


}