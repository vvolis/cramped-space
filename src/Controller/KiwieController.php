<?php
namespace App\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use DG\Twitter\Twitter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;


class KiwieController extends AbstractController
{
    /**
     * @Route("/kiwie/{id}")
     */

    public function Check()
    {
        return new JsonResponse([
            "name" => "Kiwiboy2",
            "description" => "My name is kiwyt from latvia",
            "image" => "https://cramped.space/pics/die4.png"]

        );
    }

    /**
     * @Route("/kbi")
     */
    public function KBI()
    {
        return $this->render('kbi/home.html', []);
    }

    /**
     * @Route("/kbi/pic")
     */
    public function KBIPic()
    {
        return $this->render('/kbi/pic.html', []);
    }

    /**
     * @Route("/kbi/service")
     */
    public function KbiService()
    {
        //138 otra puse
        //4 ned
        //
        $targetFrame = 138;
        $startTime = strtotime('2021-05-18');
        $targetTime = strtotime('2021-06-08');
        $secondsPerFrame = ($targetTime - $startTime) / $targetFrame;


        $now = new \DateTime();
        $now = $now->getTimestamp();

        $diff = $now - $startTime;

        $currentFrame = (round($diff / $secondsPerFrame)) % 275;


        $pr_id = sprintf("%04d", $currentFrame);
        $file =    readfile("/var/www/html/space/public/pics/kiwie-black/sek_gv005_$pr_id.png");
        $headers = array(
            'Content-Type'     => 'image/png',
            'Content-Disposition' => 'inline; filename=kiwie.png"');
        return new Response($file, 200, $headers);
    }


}