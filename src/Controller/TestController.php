<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use DG\Twitter\Twitter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use App\Utils\MarkovGenerator;


class TestController extends AbstractController
{
    private $twitter;
    private $request;

    public function __construct()
    {
        $this->request = Request::createFromGlobals();

        $accidentalSecrets = array(
            'oauth_access_token' => "1236643512642404352-pVqdlE64nGFowAPFEvxqMJBpL4DNv7",
            'oauth_access_token_secret' => "T6vxWypl0PeO2eqsxmeBkVS7l3pEpIavmWZqQsU7nzZbV",
            'consumer_key' => "xOkHBfM0LD434KqJaDeRKIIlC",
            'consumer_secret' => "ZdvZOAUAdcuurSfL5R7NYJUBt1a0AGDERggDyOFcnzvFNf77xw"
        );

        $this->twitter = new Twitter($accidentalSecrets['consumer_key'], $accidentalSecrets['consumer_secret'], $accidentalSecrets['oauth_access_token'], $accidentalSecrets['oauth_access_token_secret']);
    }

    /**
     * @Route("/vv3")
     */
    public function vv3()
    {
        $smth = true;

        $data = [];
        $user = 'Laumaryn';
        $filesystem = new Filesystem();
        $data = file_get_contents("/var/www/db/laumaryn.json");
        $data = json_decode($data);

        $dataProcessed = [];

        foreach ($data as &$tweet) {
            $dataProcessed []= preg_replace("/(?<=^|\s)@\w+/", "" , $tweet);
        }



        $gen = new MarkovGenerator();
        $gen->GenerateTable($dataProcessed);

        $output = [];
        for ($i = 0; $i <30; $i++) {
            $output []= $gen->GenerateText(280);
        }
        $output = array_filter($output);
        dump($output);

        return new Response(" ok") ;

    }




    /**
     * @Route("/vv2")
     */
    public function vv()
    {
        $gen = new MarkovGenerator();

        $text = file_get_contents("/var/www/html/space/data/vvolis_tweets.txt");

        $data = explode("\n", $text);

        $gen->GenerateTable($data);
        return new Response($gen->GenerateText(1000)) ;
    }

}