<?php
namespace App\Controller;

use App\Utils\FileDB;
use App\Utils\MarkovGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use DG\Twitter\Twitter;



class LaumarynController extends AbstractController
{

    private $twitter;
    private $userChoiceDb = null;

    public function __construct()
    {
        $this->userChoiceDb = new FileDB("laumaryn_userchoice", []);
        $settings = array(
            'oauth_access_token' => "1299793835925606401-C0T7jfo0oT6Q9tlMnCLgE5LKma2LVj",
            'oauth_access_token_secret' => "qLVqViMsUEZlzCHJPqVGDcfpR102Byr3OQxuIcdCJqUas",
            'consumer_key' => "f9PBgkRtw9VEBIVBeFUPIVS0t",
            'consumer_secret' => "BB44Fk4DQwAp24bLtqRBJWOZpEY3BXdh4xsVLKBaLa2Jarzel1"
        );
        $this->twitter = new Twitter($settings['consumer_key'], $settings['consumer_secret'], $settings['oauth_access_token'], $settings['oauth_access_token_secret']);
    }

    /**
     * @Route("/laumaryn/tweet")
     */
    public function Tweet()
    {
        $state = $this->userChoiceDb->ReadState();
        if (empty($state)) {
            $data = file_get_contents("/var/www/db/laumaryn.json");
            $data = json_decode($data);

            $dataProcessed = [];

            foreach ($data as &$tweet) {
                $dataProcessed []= preg_replace("/(?<=^|\s)@\w+/", "" , $tweet);
            }

            $gen = new MarkovGenerator();
            $gen->GenerateTable($dataProcessed);

            $output = [];
            for ($i = 0; $i <30; $i++) {
                $output []= $gen->GenerateText(280);
            }
            $output = array_filter($output);

            $this->twitter->send($output[array_rand($output)]);
        } else {
            $tweet = array_pop($state);
            $this->twitter->send($tweet);
            $this->userChoiceDb->StoreState($state);
        }

        return new JsonResponse("ok");
    }

    /**
     * @Route("/laumaryn/check")
     */
    public function Check()
    {
        $data = file_get_contents("/var/www/db/laumaryn.json");
        $data = json_decode($data);

        $dataProcessed = [];

        foreach ($data as &$tweet) {
            $dataProcessed []= preg_replace("/(?<=^|\s)@\w+/", "" , $tweet);
        }

        $gen = new MarkovGenerator();
        $gen->GenerateTable($dataProcessed);

        $output = [];
        for ($i = 0; $i <30; $i++) {
            $output []= $gen->GenerateText(280);
        }
        $output = array_filter($output);


        return $this->render('laumaryn/laumaryn.html.twig', [
            'tweets' => $output
        ]);
    }

    /**
     * @Route("/laumaryn/user_choice")
     */
    public function UserChoice()
    {
        $state = $this->userChoiceDb->ReadState();
        $tweet = $_POST['tweet'];
        if(isset($tweet)){
            $state []= $tweet;
        }
        $this->userChoiceDb->StoreState($state);

        return new JsonResponse("ok");
    }

    /**
     * @Route("/laumaryn/user_choice_check")
     */
    public function UserChoiceCheck()
    {
        $state = $this->userChoiceDb->ReadState();
        $newState = [];
        foreach ($state as $tweet) {
            $wordcount = str_word_count ( $tweet);
            if ($tweet != "Super Ultra Gay Nazi!" && $tweet != "Zieg Heil!" && $tweet != "Mein Kampf" && $wordcount > 1 ) {
                $newState []= $tweet;
            }
        }

        //$this->userChoiceDb->StoreState($newState);


        return $this->render('laumaryn/laumaryn.html.twig', [
            'tweets' => $newState
        ]);

        return new JsonResponse("ok");
    }

}