<?php

namespace App\Controller;

use App\Utils\FileDB;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use DG\Twitter\Twitter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Finder\Finder;

class NftController extends AbstractController
{
    public function __construct()
    {
    }


    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }




    function getCurlData($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $contents = curl_exec($ch);
        curl_close($ch);
        return $contents;
    }
    /**
     * @Route("/nft/vampirs/")
     */
    public function CounterFailiem()
    {

        $names = [];

        $finder = new Finder();
        $finder->files()->in("/var/www/html/space/public/pics/nft/sucker");
        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                $name = $file->getFilename();
                $names [] = explode('.',$name)[0];

            }
        }

        $names = array_unique($names);
        sort($names);

        $pictures = [];

        foreach ($names as $name) {
            $parts = explode('-',$name);
            $group = $parts[0];
            $objectName = $parts[1];

            $pictures[$group] []= ['name' => $name, 'file' => '/pics/nft/sucker/' . $name . '.png' ];
        }

        return $this->render('nft/nft.html.twig', [
            'pictures' => $pictures,
        ]);
    }


    /**
     * @Route("/nft")
     */
    public function Base()
    {
        //https://failiem.lv/u/cfyz5e8as#/view/
        return $this->render('nft/nft.html.twig', [
            'pictures' => [['name' => '1-10', 'file' => '/pics/nft/sucker/CRYPTOSUCKER1-10.png' ],
                ['name' => '1-11', 'file' => '/pics/nft/sucker/CRYPTOSUCKER1-11.png' ],
                ['name' => '1-12', 'file' => '/pics/nft/sucker/CRYPTOSUCKER1-12.png' ],
                ['name' => '1-13', 'file' => '/pics/nft/sucker/CRYPTOSUCKER1-13.png' ],
                ['name' => '1-14', 'file' => '/pics/nft/sucker/CRYPTOSUCKER1-14.png' ],
                ['name' => '1-15', 'file' => '/pics/nft/sucker/CRYPTOSUCKER1-15.png' ],
            ],
        ]);
    }


    private function MergeImage($fileA, $fileB)
    {
        $image1 = imagecreatefrompng($fileA);
        $image2 = imagecreatefrompng($fileB);
        $merged_image = imagecreatetruecolor(500, 500);

        // DEFINE MAGENTA AS THE TRANSPARENCY COLOR AND FILL THE IMAGE FIRST
        $transparentColor = imagecolorallocate($merged_image, 255, 0, 255);
        imagecolortransparent($merged_image, $transparentColor);
        imagefill($merged_image, 0, 0, $transparentColor);

        imagesavealpha($merged_image, true);

        imagealphablending($image1, false);
        imagecopy($merged_image, $image1, 0, 0, 0, 0, 500, 500);
        imagealphablending($image1, true);
        imagedestroy($image1); // FREE UP SOME MEMORY

        imagealphablending($image2, false);
        imagecopy($merged_image, $image2, 0, 0, 0, 0, 500, 500);
        imagealphablending($image2, true);
        imagedestroy($image2); // FREE UP SOME MEMORY

        imagealphablending($merged_image, false);
        imagesavealpha($merged_image, true);

        $filename = "/tmp/". rand() . ".png";
        imagecolortransparent($merged_image, $transparentColor);
        imagepng($merged_image, $filename);
        imagedestroy($merged_image);

        return $filename;

    }

    public function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
        // creating a cut resource
        $cut = imagecreatetruecolor($src_w, $src_h);
        $transparentColor = imagecolorallocate($cut, 255, 0, 255);
        imagecolortransparent($cut, $transparentColor);
        imagefill($cut, 0, 0, $transparentColor);


        // copying relevant section from background to the cut resource
        imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);

        // copying relevant section from watermark to the cut resource
        imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

        // insert cut resource to destination image
        imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
    }


    private function SortImageArray(&$imageArr)
    {
        $order = ["body" => 0, "eye" => 1, "hat" =>3];

        usort($imageArr, function ($a, $b) use ($order) {
            $pos_a = $order[substr($a,0,-5)];
            $pos_b = $order[substr($b,0,-5)];
            return $pos_a - $pos_b;
        });
    }



    /**
     * @Route("/nft/img/{images}")
     */
    public function Clear($images)
    {
        $imageArray = explode(";", $images);
        $this->SortImageArray($imageArray);


        $filename = $this->MergeImage('/var/www/html/space/public/pics/nft/' . $imageArray[0], '/var/www/html/space/public/pics/nft/' . $imageArray[1]);

        $response = new BinaryFileResponse($filename);



        return $response;

        $dest = imagecreatefrompng(  '/var/www/html/space/public/pics/nft/' . $imageArray[0]);
        imagealphablending($dest, false);

        $transparent = imagecolorallocatealpha( $dest, 0, 0, 0, 127 );
        imagefill( $dest, 0, 0, $transparent );

        for ($i = 1; $i < count($imageArray); $i++) {
            $src = imagecreatefrompng( '/var/www/html/space/public/pics/nft/' . $imageArray[$i]);

            imagealphablending($src, false);
            imagesavealpha($src, true);

            $this->imagecopymerge_alpha($dest, $src, 0, 0, 0, 0, 500, 500, 100); //have to play with these numbers for it to work for you, etc.
            header('Content-Type: image/png');


            imagedestroy($src);


        }
        imagesavealpha($dest, true);

        $filename = "/tmp/". rand() . ".png";

        imagepng($dest, $filename);
        imagedestroy($dest);










        $response = new BinaryFileResponse($filename);



        return $response;

    }
}