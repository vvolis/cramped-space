<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use DG\Twitter\Twitter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;


class SecretController extends AbstractController
{
    private $twitter;
    private $request;

    public function __construct()
    {
        $this->request = Request::createFromGlobals();

        $accidentalSecrets = array(
            'oauth_access_token' => "1236643512642404352-pVqdlE64nGFowAPFEvxqMJBpL4DNv7",
            'oauth_access_token_secret' => "T6vxWypl0PeO2eqsxmeBkVS7l3pEpIavmWZqQsU7nzZbV",
            'consumer_key' => "xOkHBfM0LD434KqJaDeRKIIlC",
            'consumer_secret' => "ZdvZOAUAdcuurSfL5R7NYJUBt1a0AGDERggDyOFcnzvFNf77xw"
        );

        $this->twitter = new Twitter($accidentalSecrets['consumer_key'], $accidentalSecrets['consumer_secret'], $accidentalSecrets['oauth_access_token'], $accidentalSecrets['oauth_access_token_secret']);
    }

    /**
     * @Route("/secret")
     */
    public function secret(LoggerInterface $logger, Request $request)
    {
        $isPost = ($this->request->getMethod() == "POST");
        if ($isPost) {
            $logger->info('I just got the logger');
            $logger->info('VV msg:' . $request->request->all()['text']);
            $logger->info('VV ip'  . $request->getClientIp());
            $this->twitter->send($request->request->all()['text']);
        }
        return $this->render('secret.html.twig',
            ['posted' => $isPost]
        );
    }

    /**
     * @Route("/tw/2")
     */
    public function TW ()
    {
        $res = '';
        $statuses = $this->twitter->request('statuses/user_timeline', 'GET', ['screen_name' => "vvolis", 'count'=>20]);
        foreach ($statuses as $status) {
            $res .= "message: " . Twitter::clickable($status) . "\n";
            $res .= "posted at " . $status->created_at . "\n";
            $res .= "posted by " .  $status->user->name . "\n";
        }

        return new Response($res);
    }

}