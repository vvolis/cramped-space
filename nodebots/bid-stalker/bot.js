/* bid-stalker generated with create-discord-bot CLI */
const Discord = require('discord.js')
const client = new Discord.Client()
const WebRequest = require('web-request')
var postData = '{"size":20,"filter":{"@type":"by_owner","address":"0x207f86e2795666365747aaf1e86a4114bebd6fab","incoming":true,"inStockOnly":false}}';



const timeout = 30000;//dev - 1000, prod - 60000;
var CHANNEL_ID = "883694318802595840"; // dev - 831204123319861258 //prod - 831236222512136262

var dict = {
    1 : {
        name: "GRAY RAINBOW",
        bid: null
    },
    2: {
        name: "PINKS",
        bid: null
    },
    3: {
        name: "BARONG",
        bid: null
    },
    4: {
        name: "COAL",
        bid: null
    },
    5: {
        name: "SKY",
        bid: null
    },
    6: {
        name: "OGLE",
        bid: null
    },
    7: {
        name: "BRIGHT",
        bid: null
    },
    8: {
        name: "THERMAL",
        bid: null
    },
    9: {
        name: "ETH",
        bid: null
    },
    10: {
        name: "META",
        bid: null
    },
    11: {
        name: "CIRCUS",
        bid: null
    },
    12: {
        name: "KIWIE BIGGIES",
        bid: null
    },



    13: {
        name: "FUZZ",
        bid: null
    },
    14: {
        name: "WASTED",
        bid: null
    },
    15: {
        name: "50-50",
        bid: null
    },

    16: {
        name: "LUCID",
        bid: null
    },
    17: {
        name: "STEPS",
        bid: null
    },
    18: {
        name: "BURNOUT",
        bid: null
    },
    19: {
        name: "FURY",
        bid: null
    },

    20: { name: "REBAR", bid: null },
    21: { name: "POOLPARTY", bid: null },
    22: { name: "FLAMMABLE", bid: null },
    23: { name: "GUARDIAN", bid: null },
    24: { name: "BEE", bid: null },
    25: { name: "RADIOACTIVE", bid: null },
    26: { name: "PIONEER", bid: null },
    27: { name: "RECEIPT", bid: null },
    28: { name: "SHIPMENT", bid: null },
}
var REQ_IN_PROGRESS = false;


async function updateAndPost() {
    var channel = client.channels.get(CHANNEL_ID);
    //channel.send('...looop');

    if (REQ_IN_PROGRESS) {
        return;
    }

    const itemPromises = [];
    for (i = 1; i <= 28; i++) {
        console.log("Fetching " + i)
        var promise1 = WebRequest.post('https://api-mainnet.rarible.com/marketplace/api/v4/items/0xa9b2d27fd81cb0a5195b5974a83cdcbde5db1af9%3A' + i + '/offers', { headers: { 'content-type': 'application/json' } }, JSON.stringify({size:1}));
        itemPromises.push(promise1);
    }

    Promise.all(itemPromises)
        .then((results) => {
            offerData = [];
            var users = [];

            results.forEach(result => {
                var offer = new Object();

                var body = JSON.parse(result.request.response.toJSON().body);
                if (body.length > 0) {
                    var highestOffer = JSON.parse(result.request.response.toJSON().body)[0];
                    //console.log(body);
                    offer.price = highestOffer.buyPriceEth;
                    offer.offerOwner = highestOffer.maker;
                    offer.tokenId = highestOffer.takeTokenId;
                    offer.createdAt = highestOffer.createdAt;
                    offerData.push(offer);
                    users.push(highestOffer.maker)
                }
            });

            //console.log(users);

            //channel.send(JSON.stringify(users));


            let promise2 = WebRequest.post('https://api-mainnet.rarible.com/marketplace/api/v4/profiles/list', { headers: { 'content-type': 'application/json' } }, JSON.stringify(users));

            promise2.then((userResult) => {
                let userData = JSON.parse(userResult.request.response.toJSON().body);
                console.log(userData);

                offerData.forEach(element => {


                    var key = element.tokenId;
                    var price = element.price;
                    var user = userData.find(usr => usr.id == element.offerOwner);
                    var name = element.offerOwner
                    if (user && user.name) {
                        name = user.name;
                    }

                    price = Math.round(price * 1000) / 1000

                    //console.log(element.offerOwner, name);

                    var txt = "";
                    var log = false;


                    console.log("Processing " + key + " price:" + price);

                    if (dict.hasOwnProperty(key) && dict[key].bid != null) {
                        if (dict[key].bid < price) {
                            txt = 'New bid for ' +  dict[key].name + ' - ' + price + 'ETH from ' + name;
                            dict[key].bid = price;
                            log = true;
                        }
                    } else {
                        txt = 'AFAIK first bid for ' +  dict[key].name + ' - ' + price + 'ETH from ' + name;
                        dict[key] = { name: dict[key].name, bid: price};
                        log = false; //|| key >= 20; //small haxx
                    }

                    if (log) {
                        //console.log(txt);
                        channel.send(txt);
                    }

                });

                REQ_IN_PROGRESS = false;

            })

        }).catch((e) => {
            console.log("error caught");
            console.log(e);
        });
}


client.on('ready', () => {
    var channel = client.channels.get(CHANNEL_ID);
    //channel.send('beep boop');
    updateAndPost();
})


client.on('message', async (msg) => {
    /*if (msg.content === 'G') {
        var data = await getData();
        
    }*/
});


client.login('ODMxMjAyODA5MzY5MTk4NjU0.YHRz8A.hDtRhfIvQtdcYLXwczQHZRPrENg');
setInterval(function () { updateAndPost() }, timeout);